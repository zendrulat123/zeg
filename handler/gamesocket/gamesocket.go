package basics

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

func Gamesockets(c echo.Context) error {

	return c.Render(http.StatusOK, "gamesocket.html", map[string]interface{}{})

}
