package basics

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

func Introduction(c echo.Context) error {

	return c.Render(http.StatusOK, "introduction.html", map[string]interface{}{})

}
