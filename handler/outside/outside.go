package basics

import (
	"fmt"
	"net/http"

	"github.com/labstack/echo/v4"
	clientguy "gitlab.com/zendrulat123/zeg/clientguy"
)

func Outside(c echo.Context) error {
	color := c.Param("color")
	name := c.Param("name")
	guys := clientguy.Clientguy{Name: name, Color: color}
	fmt.Println(guys)
	return c.Render(http.StatusOK, "outside.html", map[string]interface{}{
		"guys": guys,
	})
}
