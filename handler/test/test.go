package basics

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

func Test(c echo.Context) error {

	return c.Render(http.StatusOK, "test.html", map[string]interface{}{})

}
