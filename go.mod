module gitlab.com/zendrulat123/zeg

go 1.16

require (
	github.com/gorilla/websocket v1.4.2
	github.com/labstack/echo/v4 v4.6.1
	github.com/labstack/gommon v0.3.1
	golang.org/x/crypto v0.0.0-20211117183948-ae814b36b871 // indirect
	golang.org/x/net v0.0.0-20211118161319-6a13c67c3ce4 // indirect
	golang.org/x/sys v0.0.0-20211117180635-dee7805ff2e1 // indirect
	golang.org/x/time v0.0.0-20211116232009-f0f3c7e86c11 // indirect
)
