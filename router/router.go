package routes

import (
	"github.com/labstack/echo/v4"
	gamesockets "gitlab.com/zendrulat123/zeg/handler/gamesocket"
	home "gitlab.com/zendrulat123/zeg/handler/home"
	introduction "gitlab.com/zendrulat123/zeg/handler/introduction"
	outside "gitlab.com/zendrulat123/zeg/handler/outside"
	gamesocket "gitlab.com/zendrulat123/zeg/handler/socket/gamesocket"
	test "gitlab.com/zendrulat123/zeg/handler/test"
)

func Routes(e *echo.Echo) {

	e.GET("/t", introduction.Introduction)          //intro
	e.GET("/home", home.Home)                       //home
	e.GET("/outside/:color/:name", outside.Outside) //home
	e.GET("/ws", gamesocket.Gamesocket)
	e.GET("/", gamesockets.Gamesockets)
	e.GET("/chat", test.Test) //home

}
